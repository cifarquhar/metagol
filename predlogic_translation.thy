theory predlogic_translation
  imports Main "file_parsing"
begin


(*Test examples*)

ML{*
val test1 = @{term " \<forall>x. P x "}
*}

ML{*
Trans.term_trans test1
*}

ML{*
val test2 = @{term " \<forall>x. \<exists>y. P x y"}
*}

ML{*
Trans.term_trans test2
*}

ML{*
val test3 = @{term " \<exists>x. P x "}
*}

ML{*
Trans.term_trans test3
*}

ML{*
val test4 = @{term "\<And>x y. P x y \<Longrightarrow> \<forall>y. \<exists>x. P x y"}
*}

ML{*
 fun printvar i [] = "b(" ^ Int.toString i ^ ")"
  |  printvar i ((x,y)::xs) = if i=x then ("arb(" ^ y ^ ")") else printvar i xs;

 fun inc xs = (map (fn (x,y) => ((x+1),y)) xs)
 fun addvar y xs = (0,y)::(inc xs);

fun term_trans _ (Const (a, _)) = Trans.trans_consts a 
    | term_trans _ (Free (a, _)) = "v(" ^ a ^ ")"
    | term_trans _ (Var ((v,_), _)) = "v(" ^ v ^ ")"
    | term_trans m (Bound x) = printvar x m
    | term_trans m (Const ("HOL.Trueprop",_) $ u) = term_trans m u
    | term_trans m (Const ("_type_constraint_",_) $ u) = term_trans m u
    | term_trans m (Const ("all",_) $ Abs (n,_,b)) = term_trans (addvar n m) b
    | term_trans m (Const ("HOL.All",_) $ Abs (n,_,b)) = "forall(" ^ term_trans (inc m) b ^ ")"
    | term_trans m (Const ("HOL.Ex",_) $ Abs (n,_,b)) = "exist(" ^ term_trans (inc m) b ^ ")"
    | term_trans m (Const (c,_) $ Abs (n,_,b)) = "lambda(" ^ c ^ "," ^ term_trans (addvar n m) b ^ ")"
    | term_trans m (t $ u) = "app(" ^ term_trans m t ^ "," ^ term_trans m u ^ ")" ;

*}
ML{*
val trans4 = term_trans [] test4;
val trans1 = term_trans [] test1;
val trans2 = term_trans [] test2;
val trans3 = term_trans [] test3;
*}




(*Metagol examples*)

lemma a: "(\<exists>x. \<forall>y. P x y) \<longrightarrow> (\<forall>y. \<exists>x. P x y)"
  apply (rule impI)
  apply (erule exE)
  apply (rule allI)
  apply (erule allE)
  apply (rule exI)
  apply assumption
done

lemma b: "(\<forall>x. P x \<longrightarrow> Q) = ((\<exists>x. P x) \<longrightarrow> Q)"
  apply (rule iffI)
  apply (rule impI)
  apply (erule exE)
  apply (erule allE)
  apply (erule impE)
  apply assumption
  apply assumption
  apply (rule allI)
  apply (rule impI)
  apply (erule impE)
  apply (rule exI)
  apply assumption
  apply assumption
done

lemma c: "((\<forall> x. P x) \<and> (\<forall> x. Q x)) = (\<forall> x. (P x \<and> Q x))"
  apply (rule iffI)
  apply (erule conjE)
  apply (rule allI)
  apply (erule allE)
  apply (erule allE)
  apply (rule conjI)
  apply assumption
  apply assumption
  apply (rule conjI)
  apply (rule allI)
  apply (erule allE)
  apply (erule conjE)
  apply assumption
  apply (rule allI)
  apply (erule allE)
  apply (erule conjE)
  apply assumption
done

lemma d: "((\<exists> x. P x) \<or> (\<exists> x. Q x)) = (\<exists> x. (P x \<or> Q x))"
  apply (rule iffI)
  apply (erule disjE)
  apply (erule exE)
  apply (rule exI)
  apply (rule disjI1)
  apply assumption
  apply (erule exE)
  apply (rule exI)
  apply (rule disjI2)
  apply assumption
  apply (erule exE)
  apply (erule disjE)
  apply (rule disjI1)
  apply (rule exI)
  apply assumption
  apply (rule disjI2)
  apply (rule exI)
  apply assumption
done

lemma e: "( \<not> (\<forall> x. P x)) = (\<exists> x. \<not> P x)"
  apply (rule iffI)
  apply (rule classical)
  apply (erule notE)
  apply (rule allI)
  apply (rule classical)
  apply (erule notE)
  apply (rule exI)
  apply assumption
  apply (erule exE)
  apply (rule notI)
  apply (erule allE)
  apply (erule notE)
  apply assumption
done

end

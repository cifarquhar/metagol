(*Group theory files for translation using ProofProcess*)

theory groups_for_translation
  imports Main "file_parsing"
begin

ML{*
val gtprf1 = nth (Generator.get_nodes "grouptheory") 0 |> snd |> hd
val gtprf2 = nth (Generator.get_nodes "grouptheory") 1 |> snd |> hd
val gtprf3 = nth (Generator.get_nodes "grouptheory") 2 |> snd |> hd
val gtprf4 = nth (Generator.get_nodes "grouptheory") 3 |> snd |> hd
val gtprf5 = nth (Generator.get_nodes "grouptheory") 4 |> snd |> hd
val gtprf6 = nth (Generator.get_nodes "grouptheory") 5 |> snd |> hd
val gtprf7 = nth (Generator.get_nodes "grouptheory") 6 |> snd |> hd
val gtprf8 = nth (Generator.get_nodes "grouptheory") 7 |> snd |> hd
val gtprf9 = nth (Generator.get_nodes "grouptheory") 8 |> snd |> hd
val gtprf10 = nth (Generator.get_nodes "grouptheory") 9 |> snd |> hd
val gtprf11 = nth (Generator.get_nodes "grouptheory") 10 |> snd |> hd
val gtprf12 = nth (Generator.get_nodes "grouptheory") 11 |> snd |> hd
val gtprf13 = nth (Generator.get_nodes "grouptheory") 12 |> snd |> hd
*}


ML{*
val gttree1 = Generator.label_tree "a" 0 (gtprf1) |> snd
val gttree2 = Generator.label_tree "b" 0 (gtprf2) |> snd
val gttree3 = Generator.label_tree "c" 0 (gtprf3) |> snd
val gttree4 = Generator.label_tree "d" 0 (gtprf4) |> snd
val gttree5 = Generator.label_tree "e" 0 (gtprf5) |> snd
val gttree6 = Generator.label_tree "f" 0 (gtprf6) |> snd
val gttree7 = Generator.label_tree "g" 0 (gtprf7) |> snd
val gttree8 = Generator.label_tree "h" 0 (gtprf8) |> snd
val gttree9 = Generator.label_tree "i" 0 (gtprf9) |> snd
val gttree10 = Generator.label_tree "j" 0 (gtprf10) |> snd
val gttree11 = Generator.label_tree "k" 0 (gtprf11) |> snd
val gttree12 = Generator.label_tree "l" 0 (gtprf12) |> snd
val gttree13 = Generator.label_tree "m" 0 (gtprf13) |> snd
*}

ML{*
gttree;
*}

ML{*
val testterm = @{term "A \<longrightarrow> A"};
@{make_string}
*}

ML{*
Generator.generate_prolog_file gttree1 "grouptheory";
Generator.generate_prolog_file gttree2 "grouptheory";
Generator.generate_prolog_file gttree3 "grouptheory";
Generator.generate_prolog_file gttree4 "grouptheory";
Generator.generate_prolog_file gttree5 "grouptheory";
Generator.generate_prolog_file gttree6 "grouptheory";
Generator.generate_prolog_file gttree7 "grouptheory";
Generator.generate_prolog_file gttree8 "grouptheory";
Generator.generate_prolog_file gttree9 "grouptheory";
Generator.generate_prolog_file gttree10 "grouptheory";
Generator.generate_prolog_file gttree11 "grouptheory";
Generator.generate_prolog_file gttree12 "grouptheory";
Generator.generate_prolog_file gttree13 "grouptheory";
*}


typedecl G

axiomatization
  mult :: "G => G => G" (infixl "**" 60) and
  e :: "G" and
  inv :: "G => G"
where
 ax1: "e ** a = a" and
 ax2:  "(a ** b) ** c = a **(b ** c)" and
 ax3: "inv a ** a = e"

consts a :: G
(*consts b :: G*)
(*consts c :: G*)
(*consts f :: G*)

lemmas ax1_sym = ax1[symmetric]
lemmas ax2_sym = ax2[symmetric]
lemmas ax3_sym = ax3[symmetric]

thm ax1
 ax1[symmetric]

(*From axioms file - basic operations*)

lemma aux1: "e = inv (inv a) ** inv a"
 apply (subst ax3)
 apply (rule refl)
 done

lemma inv_rev: "a ** inv a = e"
  apply (subst ax3_sym)
  apply (subst ax1_sym)
  apply (subst aux1)
  apply (subst ax2)
  apply (subst ax3)
  apply (subst ax3)
  apply (subst ax2)
  apply (subst ax1)
  apply (subst ax3)
  apply (rule refl)
done

lemmas inv_rev_sym = inv_rev[symmetric]

lemma id_rev: "a ** e = a"
  apply (subst ax3_sym)
  apply (subst ax2_sym)
  apply (subst inv_rev)
  apply (rule ax1)
done

lemmas id_rev_sym = id_rev[symmetric]

lemma id_comm: "e ** a = a ** e"    
  apply (subst id_rev)                  
  apply (rule ax1)                     
done

lemma left_div:  "a ** b = a ** c \<Longrightarrow> b = c"
   apply (subst ax1_sym)
back
   apply (subst ax1_sym)

  apply (subst ax3_sym)
back
  apply (subst ax3_sym)

  apply (subst ax1)
 sorry
 
lemma right_div:  "b ** a = c ** a \<Longrightarrow> b = c"
  apply (subst id_rev_sym)
  apply (subst inv_rev_sym)
  apply (subst ax2_sym)
sorry

lemma inv_comm: "a ** inv a = inv a ** a"
  apply (subst ax3)
  apply (subst ax1_sym)
  apply (subst ax2)
  apply (subst inv_rev)
  apply (rule ax1)
done

lemma id_unique:   "a ** f = a \<Longrightarrow> e = f"
  apply (subst ax3_sym)
  apply (subst inv_comm)
  apply (subst id1)
  apply (rule refl)
  apply (rule refl)
sorry

lemmas id_unique_sym = id_unique[symmetric]
sorry
lemma inv_unique:  assumes inv1:"l ** a = e" shows "l = inv a"
  apply (subst id_rev_sym)
  apply (subst inv_rev_sym)
  apply (subst ax2_sym)
  apply (subst inv1)
  apply (subst ax1)
  apply (rule refl)
done
  

lemmas inv_unique_sym = inv_unique[symmetric]

lemma inv_inv: "inv (inv a) = a"
  apply (subst inv_unique_sym)
  apply (subst inv_rev_sym)
  apply (subst ax3)
  apply (subst inv_rev_sym)

sorry


lemma lat_sq:  "a ** x = b \<Longrightarrow> x = inv a ** b"

  apply (subst assms[symmetric])
  apply (subst ax2_sym)
  apply (subst ax3)
  apply (subst ax1)
  apply (rule refl)
done

lemma inv_id: "inv e = e"
  apply (subst id_rev_sym)
  apply (subst ax3)
sorry



(*Group order lemmas*)

axiomatization
  FG :: "G set"
where
 fax: "finite FG"

fun
  gexp :: "G => nat => G"
where
  "gexp g 0 = e"
| "gexp g (Suc n) = (gexp g n) ** g" 

thm gexp.simps

lemma gexp_id: "gexp e n = e"         
  apply (induct n) 
  apply (subst gexp.simps)
  apply (rule refl)                   
  apply (subst gexp.simps)                          
  apply (subst id_rev)
  apply assumption                   
done       

lemma gexp_order_plus: "gexp g n ** gexp g m = gexp g (n + m)"
  apply (induct m)
  apply (subst gexp.simps)
  apply (subst add_0_right)
  apply (rule id_rev)
  apply (subst gexp.simps)
  apply (subst add_Suc_right)
  apply (subst gexp.simps)
  apply (subst ax2_sym)
  apply (simp only: right_div)
done

lemma gexp_order_plus_comm: "gexp g (n + m) = gexp g (m + n)"
  apply (subst nat_add_commute)
  apply (rule refl)
done

lemma gexp_order_mult: "gexp (gexp g m) n = gexp g (m * n)"
  apply (induct n)
  apply (subst gexp.simps)
  apply (subst mult_0_right)
  apply (subst gexp.simps)
  apply (rule refl)
  apply (subst gexp.simps)
  apply (subst mult_Suc_right)
  apply (simp only: gexp_order_plus)
  apply (simp only: gexp_order_plus_comm)
done

lemma gexp_order_mult_comm: "gexp g (m * n) = gexp (gexp g m) n"
  apply (simp only: gexp_order_mult)
sorry

lemma gexp_order_Suc: "gexp g n ** g = gexp g (Suc n)"
  apply (induct n)
  apply (subst gexp.simps)
  apply (subst gexp.simps)
  apply (subst ax1)
  apply (subst gexp.simps)
  apply (subst ax1)
  apply (rule refl)
  apply (subst gexp.simps)
  apply (subst gexp.simps)
  apply (subst gexp.simps)
  apply (rule refl)
done

lemma gexp_inv: "inv (gexp g n) = gexp (inv g) n"
  apply (induct n)
  apply (subst gexp.simps)
  apply (subst gexp.simps)
  apply (rule inv_id)
  apply (subst gexp.simps)
  apply (subst gexp.simps)
  apply (rule inv_unique_sym)
apply (metis add_Suc_right ax2 gexp_order_Suc gexp_order_plus id_rev inv_rev lat_sq nat_add_commute)
sorry



definition 
  iexp :: "G => int => G"
where
 "iexp g n \<equiv> if n < 0 then inv (gexp g (nat (-n))) else gexp g (nat n)"


lemma iexp_neg_int: "iexp g (-n) = inv (iexp g n)"
  apply (unfold iexp_def)
  apply simp
  apply (rule conjI)
  apply (subst inv_inv)
  apply (rule impI)
  apply (rule refl)
  apply (subst inv_id)
  apply (rule impI)+
  apply (rule refl)
done

lemma shows "iexp (iexp g m) n = iexp g (m * n)"
 apply (rule int_induct[where P="\<lambda> m. iexp (iexp g m) n = iexp g (m * n)" and k=1])
  apply auto
  apply (unfold iexp_def)
  apply (simp add: ax1)
  apply simp
  apply auto
  apply (simp add: gexp_inv)
  apply (simp add: gexp_order_mult)
sorry

lemma  shows "iexp g n ** iexp g (m) = iexp g (n + m)"
  apply (rule int_induct)
  apply auto
  apply (subst iexp_def)
  apply auto
  prefer 2
  apply (subst iexp_def)
  apply auto
  prefer 2
  apply (subst gexp_order_plus)
  apply (subst iexp_def)
  apply auto

sorry



end
